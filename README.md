# Run back end
Launch a terminal and run the following command

$ cd real-time

$ ./mvnw spring-boot:run 

# Run front end
Launch another terminal and run the following command

$ cd real-time/front-end

$ spring run app.groovy -- --server.port=9000

In a Web browser, visit 
http://localhost:9000/index.html