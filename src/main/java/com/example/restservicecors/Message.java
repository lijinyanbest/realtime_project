package com.example.restservicecors;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Message {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;
  private String username;
  private String createdDateTime;
  private String content;


  protected Message() {}

  public Message(String username, String createdDateTime, String content) {
    this.username = username;
    this.createdDateTime = createdDateTime;
    this.content = content;
  }

  @Override
  public String toString() {
    return String.format(
        "User[id=%d, firstName='%s', lastName='%s']", id, username, createdDateTime);
  }

  public Long getId() {
    return id;
  }

  public String getCreatedDateTime() {
    return createdDateTime;
  }

  public String getContent() {
    return content;
  }

  public String getUsername() {
    return username;
  }

}
