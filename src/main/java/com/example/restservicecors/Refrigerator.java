package com.example.restservicecors;

import java.util.concurrent.atomic.AtomicInteger;


public class Refrigerator {

	private static AtomicInteger count = new AtomicInteger(0);

	private long id;
	
	private int temperature;
	private int turn;
	private int hours;
	private int numbers;
	private String dtime;


	public Refrigerator() {
	}

	public Refrigerator(int temperature, int turn, int hours, int numbers, String dtime) {
		id = count.incrementAndGet();
		this.temperature = temperature;
		this.turn = turn;
		this.hours = hours;
		this.numbers = numbers;
		this.dtime = dtime;
	}

	public static AtomicInteger getCount() {
		return count;
	}

	public static void setCount(AtomicInteger count) {
		Refrigerator.count = count;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public int getNumbers() {
		return numbers;
	}

	public void setNumbers(int numbers) {
		this.numbers = numbers;
	}

	public String getDtime() {
		return dtime;
	}

	public void setDtime(String dtime) {
		this.dtime = dtime;
	}
	
	

}