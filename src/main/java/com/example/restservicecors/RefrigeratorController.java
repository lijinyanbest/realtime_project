package com.example.restservicecors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Random;


import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class RefrigeratorController {

	@Autowired
  	private MessageRepository repository;


	private static List<Refrigerator> l = new ArrayList<>();
	private static int counter = 0;

	@CrossOrigin(origins = "http://localhost:9000")
	@GetMapping("/refrigerator")
	public List<Refrigerator> refrigerators(@RequestParam(required=false, defaultValue="World") String name) {
		// System.out.println("==== in refrigerator talbe  ====");
		
		int upperbound = 5;
		Random rand = new Random(); 
		int int_random = rand.nextInt(upperbound);
		int[] temperature = { -5, -3, -2, -1 , 0 };
		int random_temperature = temperature[int_random];

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
  		LocalDateTime now = LocalDateTime.now();  

		if (counter >= 2) {
			l.add(new Refrigerator(random_temperature, rand.nextInt(30), rand.nextInt(15), rand.nextInt(50), dtf.format(now)));
			counter = 0;
		}
		else {
			counter++;
		}

		if (l.size() >= 50) {
			l = new ArrayList<>();
		}

		return l;
	}

	@CrossOrigin(origins = "http://localhost:9000")
	@GetMapping("/lines")
	public List<List<Integer>> lines(@RequestParam(required=false, defaultValue="World") String name) {
		// System.out.println("==== in lines ====");

		List<Integer> data1 = new ArrayList<>();
		data1.add(30);
		data1.add(200);
		data1.add(100);
		data1.add(400);
		data1.add(150);
		data1.add(250);
		
		List<Integer> data2 = new ArrayList<>();
		data2.add(50);
		data2.add(20);
		data2.add(10);
		data2.add(40);
		data2.add(15);
		data2.add(25);

		List<List<Integer>> l = new ArrayList<>();
		l.add(data1);
		l.add(data2);

		return l;
	}

	@CrossOrigin(origins = "http://localhost:9000")
	@PostMapping("/addmessage")
	public String addMessage(@RequestParam("name") String name, @RequestParam("content") String content) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");  
  		LocalDateTime now = LocalDateTime.now();  
		repository.save(new Message(name, dtf.format(now).toString(), content));
		
		return "success";
	}

	@CrossOrigin(origins = "http://localhost:9000")
	@GetMapping("/getallmessage")
	public Iterable<Message> getAllMessage() {
		return repository.findAll();
	}

}