package com.example.restservicecors;

import java.lang.Iterable;

import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<Message, Long> {

  Iterable<Message> findAll();

  Message findById(long id);

}
